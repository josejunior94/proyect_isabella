import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from './material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogInComponent } from './log-in/log-in.component';

import {MatCheckboxModule,MatTableModule,MatDialogModule,MatIconModule } from '@angular/material';




import { MenuComponent } from './menu/menu.component';

import { CalendarComponent } from './calendar/calendar.component';
import { AreaRiservataComponent } from './area-riservata/area-riservata.component';


import { ModalComponent } from './modal/modal.component';
import { HomeComponent } from './home/home.component';
import { MioProfiloComponent } from './mio-profilo/mio-profilo.component';

import {SqlServiceService } from './sql-service.service';
import { HttpClientModule} from "@angular/common/http";

import { NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    
    
    
    AreaRiservataComponent,
   
    MenuComponent,
    
    CalendarComponent,
    
    
    ModalComponent,
    HomeComponent,
    MioProfiloComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    
    MatCheckboxModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    HttpClientModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC6TkZPZURhgG03Gl7cA2iIA0SHvlfeFfI' //apikey generata da googlemaps
    })

  ],
  providers: [AppComponent,SqlServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
