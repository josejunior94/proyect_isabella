import { NgModule} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { LogInComponent } from './log-in/log-in.component';
import { AreaRiservataComponent } from './area-riservata/area-riservata.component';



import { ModalComponent } from './modal/modal.component';
import { HomeComponent } from './home/home.component';
import { MioProfiloComponent } from './mio-profilo/mio-profilo.component';

const routes: Routes = [
  
  { path: 'login', component: LogInComponent },
  {path: 'area-riservata', component: AreaRiservataComponent, children: [
    

    { //view modal
      path: 'modal',
      component: ModalComponent,
      outlet: 'reserved'
    },
    { //view HOME
      path: 'home',
      component: HomeComponent,
      outlet: 'reserved'
    },
    { //view mio_profilo
      path: 'mio_profilo',
      component: MioProfiloComponent,
      outlet: 'reserved'
    },
    
  ] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
  constructor( ){
  }
 
  
}
