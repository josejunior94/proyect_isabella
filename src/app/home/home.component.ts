import { Component, OnInit } from '@angular/core';
import {SqlServiceService } from '../sql-service.service';
import {NgbCarouselConfig} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {


  title: string = 'My first AGM project';
  lat: number = 43.6119015;
  lng: number = 13.5225953;

  constructor(config: NgbCarouselConfig,private map:SqlServiceService) 
  
  {  //parametros del caruosel
    config.interval=5000;
    config.wrap=true;
    config.keyboard=true;
    config.pauseOnHover=false;


   }//fin del constructor

  ngOnInit() {

   /* this.map.getLocation().subscribe(data=>{

      console.log(data);
      this.lat=data.latitude;
      this.lng
      

    })
  */
  }

}
